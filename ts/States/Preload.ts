module GameTemplate {

    import Utils = Fabrique.Utils;

    export class Preload extends Fabrique.State {
        public static Name: string = 'preload';

        public name: string = Preload.Name;

        //preloader bar
        private preloaderGroup: Phaser.Group;
        private container: Phaser.Sprite;
        private bar: Phaser.Sprite;


        constructor() {
            super();
        }

        public preload(): void {
            console.log("Preloader!!!");

            this.createPreloaderArt();
            this.load.setPreloadSprite(this.bar);

            this.game.load.onLoadComplete.add(this.startGameAfterDecodingAudio, this);

            //loading assets
            Images.preloadList.forEach((assetName: string) => {
                this.game.load.image(assetName, 'assets/images/' + assetName + '.jpg');
            });

            Atlases.preloadList.forEach((assetName: string) => {
                this.game.load.atlas(assetName, 'assets/atlas/' + assetName + '.png', 'assets/atlas/' + assetName + '.json');
            });

            Utils.init(this.game);
            
            //resize during device rotation
            this.resize();
        }

        public resize(): void {
            this.game.stage.updateTransform();

            if (this.game.width > this.game.height) {
                this.elementsPositionLandscape();
            } else {
                this.elementsPositionPortrait();
            }
        }

        private elementsPositionLandscape(): void {
            Utils.setWidthOrHeightScale(this.game, true, 10, this.preloaderGroup, true);
            this.preloaderGroup.alignIn(this.world.bounds, Phaser.CENTER, 0, 0);
        }

        private elementsPositionPortrait(): void {
            Utils.setWidthOrHeightScale(this.game, true, 15, this.preloaderGroup, true);
            this.preloaderGroup.alignIn(this.world.bounds, Phaser.CENTER, 0, 0);
        }

        private createPreloaderArt(): void {
            this.preloaderGroup = this.game.add.group();
            //add preloader art
            this.container = this.game.add.sprite(0, 0, Images.PreloadContainer, 0, this.preloaderGroup);
            this.bar = this.game.add.sprite(0, 0, Images.PreloadBar, 0, this.preloaderGroup);
            this.bar.alignIn(this.container, Phaser.CENTER);
        }


        private startGameAfterDecodingAudio(): void {
            this.game.state.start(GamePlay.Name);
        }
    }
}