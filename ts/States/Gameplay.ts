module GameTemplate {

    import Utils = Fabrique.Utils;
    import Tile = Fabrique.Tile;
    import ScoreManager = Fabrique.ScoreManager;

    export class GamePlay extends Fabrique.State {
        // Default properties for this state
        public static Name: string = 'gameplay';
        public name: string = GamePlay.Name;

        // Game graphic
        private bg:Phaser.Sprite;
        private gameFieldGroup: Phaser.Group;

        private tile: Tile;
        private tileWidth:number;
        private tileHeight:number;

        private scaleSize:number;

        //gui
        private scoreField:ScoreManager;

        // Game vars
        private gameRow:number;
        private gameCol:number;

        private tilesList:Tile[][];
        private colAndRowTilesList: Phaser.Point[];
        private hiddenTilesList: Phaser.Sprite[];

        private canSelectTile: boolean;


        constructor() {
            super();

        }

        public init(num: number): void {
            console.log("Play Game!!!!");
            //scaling tiles group for fitting background
            this.scaleSize = 2.2;

            //init some vars
            this.gameRow = Constants.GAME_ROW;
            this.gameCol = Constants.GAME_COL;
            ////
            this.tilesList = [];
            this.colAndRowTilesList = [];
            this.hiddenTilesList = [];
            this.canSelectTile = true;
            ///
            //get tile size
            this.tileWidth = Utils.getImageWidth(Atlases.Tiles, "S1.jpg");
            this.tileHeight = Utils.getImageHeight(Atlases.Tiles, "S1.jpg");
        }

        public create(): void {
            super.create();

            //add background 
            this.createBackground();

            //create group for our tiles;
            this.gameFieldGroup = this.game.add.group();
            this.gameFieldGroup.scale.set(this.scaleSize);
            
            //add start field
            this.initField();

            // game field group positioning after all sprites added
            this.gameFieldGroupCenterPosition();

            //create score field
            this.createScoreField();

            //create field group mask for more fine effect appear new tiles
            this.createFieldMask();
        }
        //creating some graphics and gui
        private createBackground():void{
            this.bg = this.game.add.sprite(0, 0, Images.Background, null);
        }

        private gameFieldGroupCenterPosition():void{
            this.gameFieldGroup.alignIn(this.bg, Phaser.CENTER, 0, this.tileHeight/2);
        }

        private createScoreField():void{
            this.scoreField = new ScoreManager(this.game);
            this.game.add.existing(this.scoreField);
            this.scoreField.alignIn(this.bg, Phaser.TOP_CENTER, 0, -this.tileHeight * 2);
        }

        private createFieldMask():void{
            var mask:Phaser.Graphics = this.game.add.graphics(0, 0);

            //  Shapes drawn to the Graphics object must be filled.
            mask.beginFill(0xffffff);

            //  Here we'll draw a rectangle for group
            mask.drawRect(this.gameFieldGroup.x, this.gameFieldGroup.y, this.gameFieldGroup.width, this.gameFieldGroup.height);

            //use mask for group
            this.gameFieldGroup.mask = mask;
        }

        //create basic field
        private initField():void{
            for (let i:number = 0; i < this.gameRow; i++){
                this.tilesList[i] = [];
                for (let j:number = 0; j < this.gameCol; j++) {
                    this.tilesList[i][j] = new Tile(this.game, i, j, false, this.gameFieldGroup);
                }
            }

            //add listener to game window
            this.game.input.onDown.add(this.clickOnField, this);
        }
     
        
        private clickOnField (pointer:Phaser.Pointer) {
            // if player cant click - stop execution
            if(!this.canSelectTile){
                return;
            }

            let x: number = pointer.x - this.gameFieldGroup.x;
            let y: number = pointer.y - this.gameFieldGroup.y;


            let row = Math.floor(y / (this.tileHeight * this.scaleSize));
            let col = Math.floor(x / (this.tileHeight * this.scaleSize));

            this.checkingMatch(row, col)
                
        }

        private checkingMatch(row:number, col:number):void{
            if (row >= 0 && col >= 0 && row < this.gameRow && col < this.gameCol) {

                let selectedTile = this.tilesList[row][col];

                this.colAndRowTilesList = [];

                this.checkingNeighborTiles(selectedTile.colAndRow, selectedTile.tileType);

                //if we have match
                if (this.colAndRowTilesList.length > 1) {

                    //disable select tiles until animation will end
                    this.canSelectTile = false;
                    //if tile matching others - increase scores
                    this.scoreField.updateScore(this.colAndRowTilesList.length)
                    //and hidding our matched tiles
                    this.hiddingMatchTiles();
                }
            } 
        }

        private checkingNeighborTiles(colAndRow: Phaser.Point, tileType: number) {
            //if tiles not on field - stop execution
            if (colAndRow.x < 0 || colAndRow.y < 0 || colAndRow.x >= this.gameCol || colAndRow.y >= this.gameRow) {
                return;
            }
            if (!this.tilesList[colAndRow.y][colAndRow.x].isHide && this.tilesList[colAndRow.y][colAndRow.x].tileType == tileType && !this.checkIfPointInList(colAndRow)) {
                //if tiles are same type - put col and row in list
                this.colAndRowTilesList.push(colAndRow);
                //and checking neighbor tiles if they type match
                this.checkingNeighborTiles(new Phaser.Point(colAndRow.x + 1, colAndRow.y), tileType);
                this.checkingNeighborTiles(new Phaser.Point(colAndRow.x - 1, colAndRow.y), tileType);
                this.checkingNeighborTiles(new Phaser.Point(colAndRow.x, colAndRow.y + 1), tileType);
                this.checkingNeighborTiles(new Phaser.Point(colAndRow.x, colAndRow.y - 1), tileType);
            }
        }

        private checkIfPointInList(point: Phaser.Point) {
            //checking if point already exist in array
            for (var i = 0; i < this.colAndRowTilesList.length; i++) {
                if (this.colAndRowTilesList[i].x == point.x && this.colAndRowTilesList[i].y == point.y) {
                    return true;
                }
            }
            return false;
        }
     
        private hiddingMatchTiles() {

            for (var i:number = 0; i < this.colAndRowTilesList.length; i++) {

                this.tilesList[this.colAndRowTilesList[i].y][this.colAndRowTilesList[i].x].hideTileAnimation();

                this.hiddenTilesList.push(this.tilesList[this.colAndRowTilesList[i].y][this.colAndRowTilesList[i].x].tileSprite);

                //hidding animation
                this.tilesList[this.colAndRowTilesList[i].y][this.colAndRowTilesList[i].x].endTweenSignal.addOnce(()=>{
                    this.fillGap();
                });

                this.tilesList[this.colAndRowTilesList[i].y][this.colAndRowTilesList[i].x].isHide = true;
            }
        }
     
        private fillGap() {

            let gapMustBeFilled:boolean = false;

            for (let i:number = this.gameRow - 2; i >= 0; i--) {
                for (let j:number = 0; j < this.gameCol; j++) {

                    //if tile not hidden
                    if (!this.tilesList[i][j].isHide) {

                        //count gap in col
                        var holesBelow = this.countingGapNum(i, j);

                        //if we have gap
                        if (holesBelow > 0) {

                            gapMustBeFilled = true;
                            //moving tiles down
                            this.moveTileDown(i, j, i + holesBelow, false);
                        }
                    }


                }
            }

            if (!gapMustBeFilled) {
                // if we find no gaps - allow user to click on tiles
                this.canSelectTile = true;
            }

            for (let i:number = 0; i < this.gameCol; i++) {

                let numHolesOnTop:number = this.countingGapNum(-1, i);

                for (let j:number = numHolesOnTop - 1; j >= 0; j--) {
                    //pic hidden tile to reuse
                    let hiddenTile:Phaser.Sprite = this.hiddenTilesList.shift();

                    //add coordination above main field
                    hiddenTile.y = (j - numHolesOnTop) * this.tileHeight + this.tileHeight / 2;
                    hiddenTile.x = i * this.tileHeight + this.tileHeight / 2;

                    //return vivibility
                    hiddenTile.alpha = 1;

                    //pick new random sprite
                    let tileNewValue = Utils.getRandomNum(1, Constants.TILES_TYPE);;
                    hiddenTile.loadTexture(Atlases.Tiles, "S" + tileNewValue + ".jpg");

                    //changing tile param 
                    this.tilesList[j][i].changeParam(new Phaser.Point(i, j), hiddenTile, false, tileNewValue)

                    this.moveTileDown(0, i, j, true);
                }
            }
        }
        
        
        private countingGapNum (row:number, col:number) {
            var gapNum:number = 0;
            //counting how many gaps in col under the tile
            for (let i:number = row + 1; i < this.gameRow; i++) {
                if (this.tilesList[i][col].isHide) {
                    gapNum++;
                }
            }
            return gapNum;
        }
     
        private moveTileDown (startRow:number, starCol:number, endRow:number, moveWithoutUpdatingParam:boolean) {

            if (!moveWithoutUpdatingParam) {
                //if updating param need
                let tileToMove:Phaser.Sprite = this.tilesList[startRow][starCol].tileSprite;
                let tileValue:number = this.tilesList[startRow][starCol].tileType;


                this.tilesList[endRow][starCol].changeParam(new Phaser.Point(starCol, endRow), tileToMove, false, tileValue)
                //and hidding old tile
                this.tilesList[startRow][starCol].isHide = true;
            }

            //tween animation
            this.tilesList[endRow][starCol].moveTweenAnimation();
            this.tilesList[endRow][starCol].endTweenSignal.addOnce(()=>{
                this.canSelectTile = true;
            });
        }
     
        // function which counts tiles in a column
        private countTilesInCol(col:number) {
            let tilesNum = 0;
            for (var i:number = 0; i < this.gameRow; i++) {
                if (!this.tilesList[i][col].isHide) {
                    //calculate all unhide tiles
                    tilesNum++;
                }
            }
            return tilesNum;
        }
    }
}

