module Game {
    /**
     * The beginning, it all starts here!
     * Let's create a game class that extends phaser's default game, because this allows us to easily wrap fontand plugin loading
     */
    export class Game extends Phaser.Game {

        private basePath: string = '';

        constructor(div: string, baseUrl: string = '') {
            super({
                enableDebug: false,             //No debug
                width: Constants.GAME_WIDTH,    //Configured width
                height: Constants.GAME_HEIGHT,  //Configured height
                renderer: Phaser.AUTO,          //We prefer WebGL over canvas rendering, but we fall back to canvas because we like to support IE
                parent: div,              //The div in the html we want to put the game in, this helps with styling
                transparent: false,              //The game should be transparent
                antialias: true,                //ofcourse this is true
                forceSetTimeOut: false
            });

            this.basePath = baseUrl;

            //Here we load all the states, but they shouldn't start automaticly
            this.state.add(GameTemplate.Boot.Name, GameTemplate.Boot, false);
            this.state.add(GameTemplate.Preload.Name, GameTemplate.Preload, false);
            this.state.add(GameTemplate.GamePlay.Name, GameTemplate.GamePlay, false);

        }

        public start(): void {
            //Load the fonts
            WebFont.load(<WebFont.Config>{
                custom: <WebFont.Custom>{
                    families: ['Century Gothic'],
                    urls: [
                        this.basePath + 'assets/css/CenturyGothic.css'                        
                    ]
                },
                active: (): void => {
                    //start the game, this will happens once all fonts are loaded
                    //this way we know that game will show the correct fonts
                    this.state.start(GameTemplate.Boot.Name);
                }
            });
        }
    }


    export function getGame(div: string, baseUrl: string = ''): Game {
        return new Game(div, baseUrl);
    }

}
