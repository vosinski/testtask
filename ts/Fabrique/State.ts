module Fabrique {
    export class State extends Phaser.State {
        public static Name: string = 'default';

        public game: Phaser.Game;

        public name: string = State.Name;

    }
}