module Fabrique {

    export class Utils {
        public static game: Phaser.Game

        public static init(game: Phaser.Game) {
            Utils.game = game
        }

        //reset position and scale sprite after screen rotation
        public static setWidthOrHeightScale(game: Phaser.Game, isWidth: boolean, percentage: number, sprite: any, resetPosition: boolean = false) {
            sprite.scale.set(1, 1);
            let gameSide: number = game.width;
            let spriteSide: number = sprite.width;
            if (resetPosition === true) {
                sprite.x = 0;
                sprite.y = 0;
            }

            if (!isWidth) {
                gameSide = game.height;
                spriteSide = sprite.height;
            }
            sprite.scale.set((gameSide * percentage) / 100 / spriteSide);
            sprite.x = sprite.y = 0;
        }

        //***** get width or height from image */
        public static getImageWidth(key: string, frame?: string): number {
            return Utils.getImageFrame(key, frame).width
        }

        public static getImageHeight(key: string, frame?: string): number {
            return Utils.getImageFrame(key, frame).height
        }

        public static getImageFrame(key: string, frame?: string): Phaser.Frame {
            return (frame !== undefined)
                ? Utils.game.cache.getFrameByName(key, frame)
                : Utils.game.cache.getFrame(key)
        }
        //***** get width or height from image */

        public static getRandomNum (min:number, max:number):number {
            return Math.floor(min + Math.random() * (max + 1 - min));
        }
    }
}
