class Constants {
    //Game Dimensions
    public static GAME_WIDTH: number = 1280;
    public static GAME_HEIGHT: number = 720;
     
    //Game constats
    public static GAME_ROW: number = 5;
    public static GAME_COL: number = 6;
    public static TILES_TYPE: number = 5;
    
}