class Images {
    //All the seperate images needed in the game

    //preloader
    public static PreloadContainer: string = 'preloader_container';
    public static PreloadBar: string = 'preloader_bar';

    //other images
    public static Background: string = 'Background';
    


    /**
     * A list of all images we want preloaded before the preloader
     * So this is a list of assets we need to show the preloader itself.
     * These should be loaded in the splash screen
     */
    public static preloadList: string[] = [
        Images.Background,
    ];


    public static preloadBarList: string[] = [
        Images.PreloadContainer,
        Images.PreloadBar
    ];
}
