class Atlases {
    //All the Atlasses required by the game
    public static Tiles: string = 'tiles';

    /**
     * A list of all images we want preloaded before the preloader
     * So this is a list of assets we need to show the preloader itself.
     */
    public static preloadList: string[] = [
        Atlases.Tiles
    ];
}
