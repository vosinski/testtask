module Fabrique {
    export class ScoreManager extends Phaser.Group {
        
        private scoreField:Phaser.Text;
        private score:number;

        constructor(game:Phaser.Game){
            super(game, null, "score");
            
            this.game = game;

            this.init();
        }

        private init():void{
            this.score = 0;

            this.createScoreField();
            
        }

        private createScoreField():void{

            this.scoreField = new Phaser.Text(this.game, 0, 0, "Score: " + this.score.toString());
            // this.scoreField.anchor.set(0.5, 0.5);
            this.scoreField.fontSize = 40;
            this.scoreField.fill = '#ffffff';
            this.scoreField.strokeThickness = 2;
            this.scoreField.stroke = '#ffffff';
            this.scoreField.setShadow(1, 1, 'rgba(0,0,0,1)', 15);
            this.add(this.scoreField);
        }

        public updateScore(score:number):void{
            this.score += score;
            this.scoreField.setText("Score: " + this.score.toString());
        }
    }
}