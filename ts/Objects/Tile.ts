module Fabrique {
    export class Tile {

        public tileSprite:Phaser.Sprite;
        public colAndRow:Phaser.Point;
        public tileType:number;
        public isHide: boolean;

        private game:Phaser.Game;
        private tileWidth:number;
        private tileHeight:number;
        private parentGroup:Phaser.Group;
        public endTweenSignal:Phaser.Signal;


        constructor(game: Phaser.Game, row:number, col:number, isHide:boolean, group:Phaser.Group) {
            
            this.game = game;
            this.colAndRow = new Phaser.Point(col, row);
            this.isHide = isHide;
            this.parentGroup = group;

            this.init()

        }

        private init(): void {
            this.endTweenSignal = new Phaser.Signal();

            this.tileWidth = Utils.getImageWidth(Atlases.Tiles, "S1.jpg");
            this.tileHeight = Utils.getImageHeight(Atlases.Tiles, "S1.jpg");

            let spriteX: number = this.colAndRow.x * this.tileWidth + this.tileWidth / 2;
            let spriteY: number = this.colAndRow.y * this.tileWidth + this.tileWidth / 2;

            this.tileType = Utils.getRandomNum(1, Constants.TILES_TYPE);
            

            this.tileSprite = this.game.add.sprite(spriteX, spriteY, Atlases.Tiles, "S" + this.tileType + ".jpg");
            this.tileSprite.anchor.setTo(0.5);
            this.parentGroup.add(this.tileSprite);
        }

        public changeParam(coordinate: Phaser.Point, sprite: Phaser.Sprite, isEmpty: boolean, value: number){
            this.tileSprite = sprite;
            this.colAndRow = coordinate;
            this.tileType = value;
            this.isHide = isEmpty;
        }

        public hideTileAnimation():void{
            let alphaTween = this.game.add.tween(this.tileSprite).to({
                alpha: 0
            }, 200, Phaser.Easing.Linear.None, true);

            let tween = this.game.add.tween(this.tileSprite.scale).to({
                x:0,
                y:0
            }, 200, Phaser.Easing.Linear.None, true);

            tween.onComplete.addOnce(() => {
                
                this.tileSprite.alpha = 0;
                this.tileSprite.scale.setTo(1);

                if (tween.manager.getAll().length == 1) {
                    this.endTweenSignal.dispatch();
                }
            }, this);
        }

        public moveTweenAnimation():void{
            let tween = this.game.add.tween(this.tileSprite).to({
                y: this.colAndRow.y * this.tileHeight + this.tileHeight / 2
            }, 100, Phaser.Easing.Linear.None, true);

            tween.onComplete.addOnce(() => {
                if (tween.manager.getAll().length == 1) {
                    this.endTweenSignal.dispatch();
                }
            }, this);
        }
    }
}