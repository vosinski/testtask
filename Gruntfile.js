var crypto = require('crypto');
module.exports = function (grunt) {
    'use strict';

    grunt.initConfig({
        //Get some details from the package.json
        game: grunt.file.readJSON('package.json'),
        //Configure the connect server that will be run
        connect: {
            server: {
                options: {
                    port: 8080,
                    base: ['_build/dev', 'node_modules']
                }
            }
        },
        //Typescript settings per build
        ts: {
            options: {
                module: 'amd',
                target: 'es5',
                sourceMap: false,
                declaration: false,
                noImplicitAny:true
            },
            dev: {
                src: ['vendor/references.ts','ts/**/*.ts'],
                reference: 'vendor/references.ts',
                dest: '_build/dev/<%= game.name %>.js'
            },
            dist: {
                src: ['vendor/references.ts','ts/**/*.ts'],
                reference: 'vendor/references.ts',
                dest: '_build/dist/<%= game.name %>-<%= game.version %>.js'
            },
            /*backend: {
                options: {
                    module: '',
                    target: 'ES6'
                },
                src: ['ts/Backend/*.ts'],
                dest: '_build/dist/Backend/game.js'
            }*/
        },
        copy: {
            dev: {
                files: [
                    {expand: true, cwd: 'assets', dest: '_build/dev/assets', src: ['**/*']},
                    {expand: true, cwd: 'templates', dest: '_build/dev', src: ['index.html']}
                ]
            },
            dist: {
                files: [
                    {expand: true, cwd: 'assets/images', dest: '_build/dist/assets/images', src: ['**/*']},
                    {expand: true, cwd: 'assets/sound', dest: '_build/dist/assets/sound', src: ['**/*', '!**/*.wav']},
                    {expand: true, cwd: 'assets/css', dest: '_build/dist/assets/css', src: ['**/*']},
                    {expand: true, cwd: 'assets/fonts', dest: '_build/dist/assets/fonts', src: ['**/*']},
                    {expand: true, cwd: 'assets/atlas', dest: '_build/dist/assets/atlas', src: ['**/*']},
                    {expand: true, cwd: 'node_modules/quartz-socket/bin', dest: '_build/dist/vendor', src: ['**/*']},
                    {expand: true, cwd: 'node_modules/phaser/build', dest: '_build/dist/vendor', src: ['phaser.min.js']}
                ]
            }
        },
        watch: {
            options: {
                livereload: true
            },
            typescript: {
                files: ['ts/**/*.ts', 'vendor/**/*.d.ts'],
                tasks: ['ts:dev']
            },
            assets: {
                files: ['assets/**/*.*', 'templates/index.html'],
                tasks: ['copy:dev']
            }
        },
        uglify: {
            options: {
                compress: {
                    // sequences: true,
                    // dead_code: true,
                    // conditionals: true,
                    // booleans: true,
                    // unused: true,
                    // if_return: true,
                    // join_vars: true,
                    // drop_console: false
                },
                mangle: false,
                beautify: true
            },
            dist: {
                files: {
                    '_build/dist/<%= game.name %>.min.js': [
                        'node_modules/phaser/build/phaser.min.js',
                        'node_modules/ga-javascript-sdk/dist/GaJavaScriptSdk.js',
                        'node_modules/webfontloader/webfontloader.js',
                        '_build/dist/<%= game.name %>-<%= game.version %>.js'

                    ]
                }
            }
        },
        concat: {
            dist: {
                src: [
                    'node_modules/webfontloader/webfontloader.js',
                    '_build/dist/<%= game.name %>-<%= game.version %>.js'

                ],
                dest: '_build/dist/game.js'
            }
        },
        clean: {
            dist: ['_build/dist/*'],
            temp: ['_build/dist/<%= game.name %>-<%= game.version %>.js']
        },
        htmlbuild: {
            dist: {
                src: 'templates/dist.html',
                dest: '_build/dist/index.html',
                options: {
                    data: {
                        // Data to pass to templates
                        version: "<%= game.version %>",
                        gameName: "<%= game.name %>",
                        title: "<%= game.title %>"
                    }
                }
            }
        }
    });

    //var buildNumber = grunt.option("buildNumber");
    var buildNumber = "1.0.1";
    grunt.registerTask('writeVersion', 'Creates a version file specifying the game version for cache busting', function() {
        if (undefined === buildNumber) {
            grunt.fail.warn('Cannot run without build number parameter');
        }
        grunt.file.write('_build/dist/version.js', 'version="' + buildNumber + '";'  );
    });

    //production build, we deploy this
    grunt.registerTask('dist', [
        'clean:dist',
        'copy:dist',
        'ts:dist',
        'concat:dist',
        'clean:temp',
        'htmlbuild:dist'
    ]);

    //Development build, used for testing. Starts filewatcher and webserver
    grunt.registerTask('dev', [
        'copy:dev',
        'ts:dev',
        'connect:server',
        'watch'
    ]);

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-ts');
    grunt.loadNpmTasks('grunt-html-build');
};
